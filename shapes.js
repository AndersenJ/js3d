drawCube = function(view,x,y,z)
{
	p0 = transform(view,x,y,z);
	p1 = transform(view,x+50,y+50,z+50);
	p2 = transform(view,x+50,y+50,z+-50);
	p3 = transform(view,x+50,y+-50,z+50);
	p4 = transform(view,x+50,y+-50,z+-50);
	p5 = transform(view,x+-50,y+50,z+50);
	p6 = transform(view,x+-50,y+50,z+-50);
	p7 = transform(view,x+-50,y+-50,z+50);
	p8 = transform(view,x+-50,y+-50,z+-50);

	var grad=ctx.createLinearGradient(p0.rx,p0.ry,950,500);
	grad.addColorStop(0,"black");
	grad.addColorStop(1,"white");
	ctx.strokeStyle=grad;

	ctx.beginPath();
	ctx.moveTo(p0.rx,p0.ry);
	ctx.lineTo(950,500);
	ctx.stroke();

	ctx.strokeStyle="black";

	ctx.beginPath();
	ctx.moveTo(p1.rx,p1.ry);
	ctx.lineTo(p2.rx,p2.ry);
	ctx.lineTo(p4.rx,p4.ry);
	ctx.lineTo(p3.rx,p3.ry);
	ctx.lineTo(p1.rx,p1.ry);
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(p5.rx,p5.ry);
	ctx.lineTo(p6.rx,p6.ry);
	ctx.lineTo(p8.rx,p8.ry);
	ctx.lineTo(p7.rx,p7.ry);
	ctx.lineTo(p5.rx,p5.ry);
	ctx.stroke();

	ctx.beginPath();
	ctx.moveTo(p1.rx,p1.ry);
	ctx.lineTo(p5.rx,p5.ry);
	ctx.lineTo(p6.rx,p6.ry);
	ctx.lineTo(p2.rx,p2.ry);
	ctx.lineTo(p4.rx,p4.ry);
	ctx.lineTo(p8.rx,p8.ry);
	ctx.lineTo(p7.rx,p7.ry);
	ctx.lineTo(p3.rx,p3.ry);
	ctx.stroke();
}
