var view = {
	x : 0,
	y : 0,
	z : -2000,
	pitch: 0,
	yaw: 0,
	roll: 0
}

moveView = function(x,y,z) {
	view.x = x;
	view.y = y;
	view.z = z;
	draw();
}

rotateView = function(pitch,yaw,roll) {
	view.pitch = pitch;
	view.yaw = yaw;
	view.roll = roll;
	draw();
}


document.onkeydown = function(e) {
	switch (e.keyCode){
		case 37:
			moveView(view.x - 1,view.y,view.z);
			break;
		case 38:
			moveView(view.x,view.y - 1,view.z);
			break;
		case 39:
			moveView(view.x + 1,view.y,view.z);
			break;
		case 40:
			moveView(view.x,view.y + 1,view.z);
			break;
		case 65:
			moveView(view.x,view.y,view.z - 1);
			break;
		case 90:
			moveView(view.x,view.y,view.z + 1);
			break;
		case 100:
			rotateView(view.pitch + 1,view.yaw,view.roll);
			break;
		case 98:
			rotateView(view.pitch,view.yaw - 1,view.roll);
			break;
		case 104:
			rotateView(view.pitch,view.yaw + 1,view.roll);
			break;
		case 102:
			rotateView(view.pitch - 1,view.yaw,view.roll);
			break;
	}
}

var canvas =        document.getElementById('facecanvas');
					context = canvas.getContext('2d'),

window.onload = function() {
	go = false;
	time = (new Date).getTime();
	canvas3d = document.getElementById('test3');
	video = document.getElementById('video');
	video.addEventListener('play', function () {
		var $this = this;
		(function loop() {
			if (!$this.paused && !$this.ended) {
				context.drawImage($this,0,0);
				if (go == true)
					find();
				setTimeout(loop, 1000/30);
			}
		})();
	}, 0);
	video.play();
	var errorCallback = function(e) {
		console.log('Rejected!', e);
	};
	navigator.webkitGetUserMedia({video: true},
						  function(localMediaStream){
							  video.src = window.URL.createObjectURL(localMediaStream);
							  video.onloadedmetadata = function(e) {
							  };
						  }, errorCallback);
	var face_pos;
	var height = 80;
	var width = Math.round(80 * video.videoWidth / video.videoHeight);
	detector = new objectdetect.detector(width, height, 1.1, objectdetect.frontalface);
	find = function() {
		var coords = detector.detect(canvas, 1);
		if (coords[0]) {
			var coord = coords[0];
			console.log(coords);

			//rescale coordinates from detector to video coords
			coord[0] *= video.videoWidth / detector.canvas.width;
			coord[1] *= video.videoHeight / detector.canvas.height;
			coord[2] *= video.videoWidth / detector.canvas.width;
			coord[3] *= video.videoHeight / detector.canvas.height;

			//find coordinates with max confidence
			var coord = coords[0];
			for (var i = coords.length - 1; i >= 0; --i)
			if (coords[i][4] > coord[4]) coord = coords[i];

			//do stuff
			var face_pos = [coord[0] + coord[2] / 2, coord[1] + coord[3] / 2];
			console.log("face_pos: ", face_pos);
		}
	}
	if (canvas3d.getContext) {
		ctx = canvas3d.getContext('2d');

		draw = function() {
			ctx.clearRect(0,0,canvas3d.width,canvas3d.height);
			drawCube(view,80,180,-200);
			drawCube(view,-210,80,310);
			drawCube(view,400,0,0);
			drawCube(view,500,400,600);
			drawCube(view,100,-400,900);
			console.log(Math.round(1000/((new Date).getTime() - time)));
			time = (new Date).getTime();
		}
		draw();
	}

}
