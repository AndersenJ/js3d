var view = {
	x : 0,
	y : 0,
	z : -2000,
	pitch: 0,
	yaw: 0,
	roll: 0
}

moveView = function(x,y,z) {
	view.x = x;
	view.y = y;
	view.z = z;
	console.log(x,y,z);
}

rotateView = function(pitch,yaw,roll) {
	view.pitch = pitch;
	view.yaw = yaw;
	view.roll = roll;
	draw();
}


document.onkeydown = function(e) {
	switch (e.keyCode){
		case 37:
			moveView(view.x - 1,view.y,view.z);
			break;
		case 38:
			moveView(view.x,view.y - 1,view.z);
			break;
		case 39:
			moveView(view.x + 1,view.y,view.z);
			break;
		case 40:
			moveView(view.x,view.y + 1,view.z);
			break;
		case 65:
			moveView(view.x,view.y,view.z - 1);
			break;
		case 90:
			moveView(view.x,view.y,view.z + 1);
			break;
		case 100:
			rotateView(view.pitch + 1,view.yaw,view.roll);
			break;
		case 98:
			rotateView(view.pitch,view.yaw - 1,view.roll);
			break;
		case 104:
			rotateView(view.pitch,view.yaw + 1,view.roll);
			break;
		case 102:
			rotateView(view.pitch - 1,view.yaw,view.roll);
			break;
	}
}

window.onload = function() {
	time = (new Date).getTime();
	canvas = document.getElementById('test4');

	var cameraResolution = new nxtjs.Rectangle( 0, 0, 640, 480);
	var brfResolution = new nxtjs.Rectangle( 0, 0, 640, 480);
	var brfRoi = new nxtjs.Rectangle( 0, 0, 640, 480);
	var faceDetectionRoi = new nxtjs.Rectangle( 0, 0, 640, 480);
	var screenRect = new nxtjs.Rectangle(0,0,640,480);
	var maskContent = true;
	var _stage = new createjs.Stage("_stage");
	_stage.enableMouseOver(10);
	createjs.Ticker.setFPS(60);
	createjs.Ticker.addEventListener("tick", _stage);
	var example = new nxtjs.ExampleFaceDetection(
		cameraResolution, brfResolution, brfRoi,
		faceDetectionRoi, screenRect, maskContent
	);
	_stage.addChild(example);

	if (canvas.getContext) {
		ctx = canvas.getContext('2d');

		draw = function() {
			ctx.clearRect(0,0,canvas.width,canvas.height);
			drawCube(view,80,180,-200);
			drawCube(view,-210,80,310);
			drawCube(view,400,0,0);
			drawCube(view,500,400,600);
			drawCube(view,100,-400,900);
			drawCube(view,200,300,1400);
			console.log(Math.round(1000/((new Date).getTime() - time)));
			time = (new Date).getTime();
		}
		draw();
	}


}
