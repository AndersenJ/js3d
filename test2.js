var view = {
	x : 0,
	y : 0,
	z : -2000,
	pitch: 0,
	yaw: 0,
	roll: 0
}

moveView = function(x,y,z) {
	view.x = x;
	view.y = y;
	view.z = z;
	draw();
}

rotateView = function(pitch,yaw,roll) {
	view.pitch = pitch;
	view.yaw = yaw;
	view.roll = roll;
	draw();
}


document.onkeydown = function(e) {
	switch (e.keyCode){
		case 37:
			moveView(view.x - 1,view.y,view.z);
			break;
		case 38:
			moveView(view.x,view.y - 1,view.z);
			break;
		case 39:
			moveView(view.x + 1,view.y,view.z);
			break;
		case 40:
			moveView(view.x,view.y + 1,view.z);
			break;
		case 65:
			moveView(view.x,view.y,view.z - 1);
			break;
		case 90:
			moveView(view.x,view.y,view.z + 1);
			break;
		case 100:
			rotateView(view.pitch + 1,view.yaw,view.roll);
			break;
		case 98:
			rotateView(view.pitch,view.yaw - 1,view.roll);
			break;
		case 104:
			rotateView(view.pitch,view.yaw + 1,view.roll);
			break;
		case 102:
			rotateView(view.pitch - 1,view.yaw,view.roll);
			break;
	}
}

window.onload = function() {
	time = (new Date).getTime();
	canvas = document.getElementById('test2');
	var face = new tracking.ObjectTracker(['face']);
	face.on('track', function(event) {
		if (event.data.length === 0) {
			//nothing detected
			console.log('nothing');
		} else {
			event.data.forEach(function(rect) {
				moveView(1200  - rect.x * 10, -1200 + rect.y * 10, -2000);
			}); 
		}
	});
	tracking.track('#video', face, { camera: true });

	if (canvas.getContext) {
		ctx = canvas.getContext('2d');

		draw = function() {
			ctx.clearRect(0,0,canvas.width,canvas.height);
			drawCube(view,80,180,-200);
			drawCube(view,-210,80,310);
			drawCube(view,400,0,0);
			drawCube(view,500,400,600);
			drawCube(view,100,-400,900);
			console.log(Math.round(1000/((new Date).getTime() - time)));
			time = (new Date).getTime();
		}
		draw();
	}


}
